package nxu.it.service;

public interface GreetingService {
    String sayHello(String name);
}
