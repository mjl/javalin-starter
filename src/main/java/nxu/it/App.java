package nxu.it;

import io.avaje.inject.BeanScope;
import io.javalin.Javalin;
import nxu.it.service.GreetingService;

public class App {
    public static void main(String[] args) {
        BeanScope beanScope = BeanScope.builder().build();
        Javalin app = beanScope.get(Javalin.class);
        app.start();

    }
}
