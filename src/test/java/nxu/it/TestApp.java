//package nxu.it;
//
//import com.mashape.unirest.http.HttpResponse;
//import com.mashape.unirest.http.Unirest;
//import com.mashape.unirest.http.exceptions.UnirestException;
//import io.avaje.inject.Bean;
//import io.avaje.inject.BeanScope;
//import io.avaje.inject.BeanScopeBuilder;
//import io.avaje.inject.test.InjectTest;
//import io.avaje.inject.test.Setup;
//import io.javalin.Javalin;
//import jakarta.inject.Inject;
//import nxu.it.service.GreetingService;
//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//@InjectTest
//public class TestApp {
//
//
//    @Setup
//    void init(BeanScopeBuilder builder) {
//        BeanScope beanScope = builder.build();
//        Javalin app = beanScope.get(Javalin.class);
//        app.start();
//    }
//
//
//    @Test
//    public void test_hello() throws UnirestException {
//        HttpResponse<String> response = Unirest.get("http://localhost:8080/").asString();
//        assertThat(response.getStatus())
//                .isEqualTo(200);
//        assertThat(response.getBody())
//                .contains("it works");
//
//    }
//}
