package nxu.it.service.impl;

import jakarta.inject.Singleton;
import nxu.it.service.GreetingService;

@Singleton
public class GreetingServiceImpl implements GreetingService {
    @Override
    public String sayHello(String name) {
        return String.format("你好, %s", name != null ? name : "无名氏");
    }
}
