package nxu.it.controller;

import io.avaje.http.api.Controller;
import io.avaje.http.api.Get;
import io.avaje.http.api.MediaType;
import io.avaje.http.api.Produces;
import io.javalin.http.Context;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import nxu.it.model.ApiResult;
import nxu.it.service.GreetingService;

import java.util.Map;

@Controller("/")
@Singleton
public class MainController {
    @Inject
    GreetingService greetingService;

    @Get("/")
    @Produces(MediaType.TEXT_HTML + ";charset=UTF-8")
    public String index(String name) {
        return "<h1>Yes, it works!</h1>";
    }

    @Get("/hello")
    @Produces(MediaType.TEXT_HTML + ";charset=UTF-8")
    public String hello(String name) {
        return greetingService.sayHello(name);
    }


    @Get("/json")
    public ApiResult json() {
        return ApiResult.ok("请求成功");
    }

    @Get("/render")

    public void render(Context ctx) {
        ctx.contentType(MediaType.TEXT_HTML + ";charset=UTF-8")
                .render("/templates/test.ftl", Map.of("name", "amigo"));

    }
}
