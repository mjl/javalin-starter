<#-- @ftlvariable name="name" type="java.lang.String" -->
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/static/styles/test.css">
    <title>Document</title>
</head>
<body>
    <h1>你好，<span class="name">${name}</span>!</h1>
    <p>这个页面内容是使用freemarker模板生成的</p>
</body>
</html>