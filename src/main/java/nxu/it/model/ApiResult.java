package nxu.it.model;

public class ApiResult {
    private int code = 200;
    private boolean success = true;
    private String message;


    public ApiResult() {
    }

    public ApiResult(int code, boolean success, String message) {
        this.code = code;
        this.success = success;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public ApiResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public static ApiResult ok(String message) {
        return ok(200, message);
    }

    public static ApiResult ok(int code, String message) {
        return new ApiResult(code, true, message);
    }

    public static ApiResult fail(String message) {
        return fail(400, message);
    }

    public static ApiResult fail(int code, String message) {
        return new ApiResult(code, false, message);
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
